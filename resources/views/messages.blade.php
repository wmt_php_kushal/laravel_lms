@if ($errors->any())
    <div class="alert alert-danger  d-flex justify-content-center ">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{{--dd($message);--}}
@if(Session::has('message'))
    <div class="Success alert-success d-flex justify-content-center">
        {{Session::get('message')}}
    </div>
@endif
