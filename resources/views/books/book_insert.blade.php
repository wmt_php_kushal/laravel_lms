@extends('layouts.app')
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/books_insert_validation.js') }}" ></script>
@endsection
@include('messages')

    @section('content')
        <div class="container">
        <form method="post" action="{{route('book.store')}}" id="insform">
            @csrf

            <div class="form-group">
                <label for="">Title</label><span>*</span>
                <input type="text" class="form-control" name="title" id="title" aria-describedby="helpId" placeholder="">
                <span></span>
            </div>
            <div class="form-group">
                <label for="">Book Author</label><span>*</span>
                <select class="custom-select" name="bookauthor" id="bookauthor">
                 <option hidden>Select bookauthor</option>
                    {{$a=\App\Author::all()}}
                    @foreach($a as $aa)
                        <option value="{{$aa->id}}">{{$aa->fullname}}</option>
                    @endforeach
                </select>
                <span></span>
            </div>
            <div class="form-group">
                <label for="">Language</label><span>*</span>
              <input type="text" class="form-control" name="language" id="language" aria-describedby="helpId" placeholder="">
              <span></span>
            </div>
            <div class="form-group">
                <label for="">Pages</label><span>*</span>
              <input type="text" class="form-control" name="pages" id="pages" aria-describedby="helpId" placeholder="">
              <span></span>
            </div>
            <div class="form-group">
                <label for="">Cover Image</label><span>*</span>
              <input type="file" class="form-control" name="coverimage" id="coverimage" aria-describedby="helpId" placeholder="">
              <span></span>
            </div>

            <div class="form-group">
                <label for="">ISBN No.</label><span>*</span>
              <input type="integer" class="form-control" name="isbn_no" id="isbn_no" aria-describedby="helpId" placeholder="">
              <span></span>
            </div>
            <div class="form-group">
                <label for="">Description</label>
                <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                <span></span>
            </div>
            <button type="submit" class="btn btn-primary"name="btninsert">Insert</button>
            </form>
            </div>
    @endsection
