@extends('layouts.app')
@section('content')
<div class="container">
    <div>
        <a class="btn btn-primary" name="insert" href="{{route('book.create')}}">INSERT</a>
    </div>
    <table class='table table-striped table-dark'>
        <tr>
            <th>Title</th>
            <th>Book Author</th>
            <th>Coverimage</th>
            <th>Language</th>
            <th>Pages</th>
            <th>Isbn No</th>
            <th>description</th>
            <th>status</th><th>Update</th><th>Delete</th><th>Home</th><th>Home</th></tr>

        @foreach( $book as $book)
            <tr>
                <td>{{$book->Title}}</td>
                <td>{{$book->bookauthor}}</td>
                <td><img src="{{asset('storage/app/'.$book->cover_image)}}">
                    </td>
                <td>{{$book->language}}</td>
                <td>{{$book->pages}}</td>
                <td>{{$book->isbn_no}}</td>
                <td>{{$book->description}}</td>
                <td>{{$book->status}}</td>

{{--                <td><a class='btn btn-primary' name='insert' href='/book/{{$book->id}}/edit/'>Update</a></td>--}}
                <td><a class='btn btn-primary' name='insert' href="{{route('book.edit',['book'=>$book->id])}}">Update</a></td>
                <td>
                    <form action='{{route('book.destroy',['book'=>$book->id])}}' method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger" >Delete</button>
                    </form>
                </td>
                <td><a class='btn btn-primary' name='show' href="{{route('book.show',['book'=>$book->id])}}">Show</a></td>
                <td>@if($book->status==1)
                        <form action='/book/onoff/{{$book->id}}' method="post">
                            @csrf
                            @method('put')
                            <button type="submit" class="btn btn-success">Active</button>
                        </form>
                        @endif
                    @if($book->status==0)
                        <form action='/book/onoff/{{$book->id}}' method="post">
                            @csrf
                            @method('put')
                            <button type="submit" class="btn btn-danger">Inactive</button>
                        </form>
                    @endif
                </td></tr>
        @endforeach
    </table>

</div>
@endsection
