@extends('layouts.app')
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/books_update_validation.js') }}" defer></script>
@endsection
@include('messages')

@section('content')
    <div class="container">
        <form method="post" action="{{route('book.update',[$book->id])}}" id="updateform" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="">Title</label><span>*</span>

                <input type="text" class="form-control" name="title" id="title" value="{{$book->Title}}" aria-describedby="helpId" placeholder="">
                <span></span>
            </div>
            <div class="form-group">
                <label for="">Book Author</label><span>*</span>
                <select class="custom-select" name="bookauthor" id="bookauthor">
                    <option hidden>Select bookauthor</option>
                    <option value="{{$book->bookauthor}}" selected>{{\App\Author::find($book->bookauthor)->fullname}}</option>
                    {{$a= \App\Author::all()->except($book->bookauthor)}}
                    @foreach($a as $a)
                        <option value="{{$a->id}}">{{$a->fullname}}</option>
                    @endforeach
                </select>
                <span></span>
            </div>
            <div class="form-group">
                <label for="">Language</label><span>*</span>
                <input type="text" class="form-control" name="language" id="language" value="{{$book->language}}" aria-describedby="helpId" placeholder="">
                <span></span>
            </div>
            <div class="form-group">
                <label for="">Pages</label><span>*</span>
                <input type="text" class="form-control" name="pages" id="pages" value="{{$book->pages}}" aria-describedby="helpId" placeholder="">
                <span></span>
            </div>
            <div class="form-group">
                @if(Storage::disk('local')->has($book['title'].'-'.$book['id'].'.jpg'))
                    <section class="row">
                        <div class="col-md-4">
                            <img src="{{route('account.image',['filename'=>$book->title . '-'. $book->id()])}}">
                        </div>
                    </section>
                @endif
                <label for="">Cover Image</label><span>*</span>
                <input type="file" class="form-control" name="coverimage" id="coverimage" value="{{$book->coverimage}}" aria-describedby="helpId" placeholder="">
                <span></span>
            </div>

            <div class="form-group">
                <label for="">ISBN No.</label><span>*</span>
                <input type="text" class="form-control" name="isbn_no" value="{{$book->isbn_no}}" id="isbn_no" aria-describedby="helpId" placeholder="">
                <span></span>
            </div>
            <div class="form-group">
                <label for="">Description</label>
                <textarea class="form-control" name="description" id="description" rows="3">{{$book->description}}</textarea>
                <span></span>
            </div>
            <button type="submit" class="btn btn-primary"name="btninsert">Insert</button>
        </form>
    </div>

@endsection
