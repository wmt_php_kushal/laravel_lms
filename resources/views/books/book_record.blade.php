@extends('layouts.app')
@section('content')
<div class="container">
    <div>
        <a class="btn btn-primary" name="insert" href="{{route('book.create')}}">INSERT</a>
    </div>
    <table class='table table-striped table-dark'>
        <tr>
            <th>Title</th>
            <th>Book Author</th>
            <th>Coverimage</th>
            <th>Language</th>
            <th>Pages</th>
            <th>Isbn No</th>
            <th>description</th>
            <th>status</th><th>Update</th><th>Delete</th><th>Home</th></tr>

            <tr>
                <td>{{$book->Title}}</td>
                <td>{{$book->bookauthor}}</td>
                <td><img src="{{$book->coverimage}}"></td>
                <td>{{$book->language}}</td>
                <td>{{$book->pages}}</td>
                <td>{{$book->isbn_no}}</td>
                <td>{{$book->description}}</td>
                <td>{{$book->status}}</td>
{{--                <td><a class='btn btn-primary' name='insert' href='/book/{{$book->id}}/edit/'>Update</a></td>--}}
                <td><a class='btn btn-primary' name='insert' href="{{route('book.edit',['book'=>$book->id])}}">Update</a></td>
                <td>
                    <form action='{{route('book.destroy',['book'=>$book->id])}}' method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger" >Delete</button>
                    </form>
                </td>
                <td><a class='btn btn-primary' name='show' href="{{route('book.index',['book'=>$book->id])}}">Home</a></td></tr>
    </table>

</div>
@endsection
