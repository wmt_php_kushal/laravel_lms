@extends('layouts.app')
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/author_update_validations.js') }}" defer></script>
@endsection
@include('messages')

    @section('content')
    <div class="container sf">
{{--        <form method="post" action="/author/{$author->id}" id="insform">--}}
        <form method="post" action="{{route('author.update',[$author->id])}}" id="updateform">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="">Firstname</label><span>*</span>
                <input type="text" class="form-control" name="fullname" id="fullname" value="{{$author->fullname}}" aria-describedby="helpId" placeholder="">
                <span id="fnameErr"></span>
            </div>

            <div class="form-group">
                <label for="">DOB</label><span>*</span>
                  <input type="date" class="form-control" name="dob" id="dob" value="{{$author->dob}}" aria-describedby="helpId" placeholder="">
                  <span id="dobErr"></span>

            </div>

            <div class="form-group">
                <label for="">Gender</label><span>*</span>
                    <select class="custom-select" name="gender" id="gender">
                        <option selected value="{{$author->gender}}">{{$author->gender}}</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                    <span id="genderErr"></span>

            </div>
            <div class="form-group">
                <label for="">Address</label><span>*</span>
                  <textarea class="form-control" name="address" id="address" rows="3">{{$author->address}}</textarea>
                  <span id="addressErr"></span>
            </div>
            <div class="form-group">
                <label for="">Phone</label><span>*</span>
                  <input type="number" class="form-control" name="phone" value="{{$author->phone}}" id="phone" aria-describedby="helpId" placeholder="">
                  <span id="phoneErr"></span>
            </div>
            <div class="form-group">
                <label for="">Description</label>
                <textarea class="form-control" name="desc" id="desc" rows="3">{{$author->description}}</textarea>
                <span id="descErr"></span>
            </div>
            <button type="submit" class="btn btn-primary"name="btnupdate">Update</button>
            </form>
    </div>
    @endsection
