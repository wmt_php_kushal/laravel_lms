@extends('layouts.app')
@section('content')
<div class="container">
    <div>
        <a class="btn btn-primary" name="insert" href="{{route('author.create')}}">INSERT</a>
    </div>
    <table class='table table-striped table-dark'>
        <tr>
            <th>Fullname</th>
            <th>DOB</th>
            <th>GENDER</th>
            <th>address</th>
            <th>phone</th>
            <th>description</th>
            <th>status</th><th>Update</th><th>delete</th><th>Show</th><th>Active/Inactive</th></tr>

        @foreach( $author as $author)
            <tr>
                <td>{{$author->fullname}}</td>
                <td>{{$author->dob}}</td>
                <td>{{$author->gender}}</td>
                <td>{{$author->address}}</td>
                <td>{{$author->phone}}</td>
                <td>{{$author->description}}</td>
                <td>{{$author->status}}</td>
{{--                <td><a class='btn btn-primary' name='insert' href='/author/{{$author->id}}/edit/'>Update</a></td>--}}
                <td><a class='btn btn-primary' name='insert' href="{{route('author.edit',['author'=>$author->id])}}">Update</a></td>

                <td>
                    <form action='{{route('author.destroy',['author'=>$author->id])}}' method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger " >Delete</button>
                    </form>
                </td>
                <td><a class='btn btn-primary' name='show' href="{{route('author.show',['author'=>$author->id])}}">Show</a></td>
                <td>@if($author->status==1)
                        <form action='/author/onoff/{{$author->id}}' method="post">
                            @csrf
                            @method('put')
                            <button type="submit" class="btn btn-success">Active</button>
                        </form>
                    @endif
                    @if($author->status==0)
                        <form action='/author/onoff/{{$author->id}}' method="post">
                            @csrf
                            @method('put')
                            <button type="submit" class="btn btn-danger">Inactive</button>
                        </form>
                    @endif
                </td></tr>
        @endforeach
    </table>

</div>
@endsection
