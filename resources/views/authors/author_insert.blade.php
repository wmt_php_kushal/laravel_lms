@extends('layouts.app')
@section('scripts')
    <script src="{{ asset('js/author_insert_validations.js') }}" defer></script>

@endsection
@include('messages')
    @section('content')
    <div class="container sf">
        <form method="post" action="{{route('author.store')}}" id="insform">
@csrf
            <div class="form-group">
                <label for="">Full name</label><span>*</span>
                <input type="text" class="form-control" name="fullname" id="fullname" aria-describedby="helpId" placeholder="">
            </div>

            <div class="form-group">
                <label for="">DOB</label><span>*</span>
                  <input type="date" class="form-control" name="dob" id="dob" aria-describedby="helpId" placeholder="">

            </div>

            <div class="form-group">
                <label for="">Gender</label><span>*</span>
                    <select class="custom-select" name="gender" id="gender">
                        <option hidden>Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>

            </div>
            <div class="form-group">
                <label for="">Address</label><span>*</span>
                  <textarea class="form-control" name="address" id="address" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="">Phone</label><span>*</span>
                  <input type="text" class="form-control" name="phone" id="phone" aria-describedby="helpId" placeholder="">
            </div>
            <div class="form-group">
                <label for="">Description</label>
                <textarea class="form-control" name="description" id="description" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary"name="btninsert">Insert</button>
            </form>
    </div>
    @endsection
