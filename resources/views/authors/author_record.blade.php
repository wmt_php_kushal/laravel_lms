@extends('layouts.app')
@section('content')
<div class="container">

    <table class='table table-striped table-dark'>
        <tr>
            <th>Fullname</th><td>{{$author->fullname}}</td></tr>
        <tr><th>DOB</th> <td>{{$author->dob}}</td></tr>
            <tr><th>GENDER</th><td>{{$author->gender}}</td></tr>
            <tr><th>address</th><td>{{$author->address}}</td></tr>
            <tr><th>phone</th><td>{{$author->phone}}</td></tr>
            <tr><th>description</th><td>{{$author->description}}</td></tr>
            <tr><th>status</th><td>{{$author->status}}</td></tr>
        <tr><td>Insert</td><td colspan="2"><div class="col-sm-4 d-flex flex-column justify-content-center"> <a class='btn btn-primary' name='insert' href="{{route('author.edit',['author'=>$author->id])}}">Update</a>
                </div></td></tr>
        <tr colspan="2"><td>Delete</td><td><div class="col-sm-4 d-flex flex-column justify-content-center">
                    <form action='{{route('author.destroy',['author'=>$author->id])}}' method="post">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger " >Delete</button>
                    </form></div></td></tr>

        <tr colspan="2"><td>home</td><td colspan="2"><div class="col-sm-4 d-flex flex-column justify-content-center">
        <a class='btn btn-primary' name='insert' href="{{route('author.index',['author'=>$author->id])}}">Home</a></div></td></tr>

            <tr>





    </table>

</div>
@endsection
