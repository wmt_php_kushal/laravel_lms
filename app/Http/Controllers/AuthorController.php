<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $author=Author::all();
        return view('authors.author_show',['author'=>$author]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('authors.author_insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fullname'=>'required',
            'dob'=>'required|Date',
            'phone'=>'numeric|required',
            'gender'=>'required' ,
            'address'=>'required',
            'description'=>'required',
        ]);

        Author::create($request->all());
        return redirect()->route('author.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
//        Author::create($request->all());
        return view('authors.author_record',['author'=>Author::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author=Author::find($id);
        return view('authors.author_update',['author'=>$author]);
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fullname'=>'required',
            'dob'=>'bail|required|Date',
            'phone'=>'bail|required|numeric|digits:10',
            'gender'=>'required' ,
            'address'=>'required',
            'description'=>'required',
        ]);
        $a= Author::find($id);
        $a->update($request->all());
        return redirect()->route('author.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $a=Author::find($id);
        $a->delete();
        return redirect()->route('author.index');
        //
    }
    public function onoff($id){
        $a=Author::find($id);
        if($a->status==1){
            $a->status=0;
            $a->update();
        }else{
            $a->status=1;
            $a->update();
        }
//        dd($b);
        return redirect()->route('author.index');
    }
}
