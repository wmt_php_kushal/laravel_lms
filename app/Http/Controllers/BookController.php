<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book=Book::all()->where('created_by',Auth::id());
        return view('books.book_show',['book'=>$book]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.book_insert',['$a'=>Author::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' =>'required',
            'bookauthor' =>'required',
            'language'=>'required|string',
            'coverimage'=>'required',
            'pages'=>'required|numeric',
            'isbn_no'=>'required'
            ,'description'=>'required'
        ]);
        $b =  new Book;
        $file =$request->file('coverimage');
        $originalname = $file->getClientOriginalName();
        $filename= $file->storeAs('CoverImage/',$originalname);

        $b->title=$request->title;
        $b->bookauthor=$request->bookauthor;
        $b->language=$request->language;
        $b->coverimage=$filename;
        $b->pages=$request->pages;
        $b->isbn_no=$request->isbn_no;
        $b->description=$request->description;
        $b->created_by=Auth::id();
        $b->save();
        return redirect()->route('book.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('books.book_record',['book'=>Book::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book=Book::find($id);
//        dd($book);
        return view('books.book_update',['book'=>$book]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' =>'required',
            'bookauthor' =>'required',
            'language'=>'required|string',
            'coverimage'=>'required',
            'pages'=>'required |numeric',
            'isbn_no'=>'required|Digits:10'
            ,'description'=>'required'
        ]);
        $b= Book::find($id);
        $file =$request->file('coverimage');
        $originalname = $request['isbn_no'].'.'.$file->getClientOriginalExtension() ;
        $filename= $file->storeAs('CoverImage',$request['title'].''. $originalname);
        $b->title=$request->title;
        $b->bookauthor=$request->bookauthor;
        $b->language=$request->language;
        $b->coverimage=$filename;
        $b->pages=$request->pages;
        $b->isbn_no=$request->isbn_no;
        $b->description=$request->description;
        $b->update();

        return redirect()->route('book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $b=Book::find($id);
        $b->delete();
        return redirect()->route('book.index');
    }
    public function onoff($id){
        $b=Book::find($id);
        if($b->status==1){
            $b->status=0;
            $b->update();
        }else{
            $b->status=1;
            $b->update();
        }
//        dd($b);
        return redirect()->route('book.index');
    }
}
