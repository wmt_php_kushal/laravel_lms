<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    //
    protected $fillable=[
       'id', 'fullname',	'dob',	'phone',	'gender',	'address',	'description',
    ];
    public function authors(){
        return $this->hasMany(Book::class);
}
}
