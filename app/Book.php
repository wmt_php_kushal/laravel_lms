<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class Book extends Model
{
    //
    protected $fillable=[
        'title','bookauthor','language','coverimage','pages','isbn_no','description',
    ];
    public function author(){
        return $this->belongsTo(Author::class);
    }
    public function setcoverimageAttribute($filename){
//        $file=Storage::url($filename);
//        return new Response($file,200);
        if($filename){
//            $image = Storage::disk('local')->get('storage/app/'.$filename);
            $image = Storage::get(asset('storage/app/'.$filename));
            return Response($image, 200);
        }
    }
}
