<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Title');
            $table->unsignedBigInteger('bookauthor');
            $table->string('coverimage');
            $table->string('language');
            $table->integer('pages');
            $table->integer('isbn_no');
            $table->string('description');
            $table->boolean('status')->default(1);
            $table->unsignedBigInteger('created_by');
            $table->foreign('bookauthor')->references('id')->on('authors')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('user')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
