$(document).ready(function() {

    $('#updateform').validate({
        rules:{
            title: {
                required : true
            },
            pages:{
                required: true
            },
            language:{
                required: true,

            },
            bookauthor:{
                required: true
            },
            coverimage:{
                required: true
            },
            isbn_no:{
                required: true,

            },
            description:{
                required: true
            }
        }
    })
});
