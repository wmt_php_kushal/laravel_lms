$(document).ready(function($) {
    console.log('1');
    $('#insform').validate({
        rules:{
            fullname: {
                required : true
            },
            dob:{
                required: true,
                date: true
            },
            gender:{
                required: true
            },
            address:{
                required: true
            },
            phone:{
                required: true
            },
            description:{
                required: true
            }
        }

    })

});
