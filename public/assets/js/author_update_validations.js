$(document).ready(function() {
    console.log('2');

    $('#updateform').validate({
        rules:{
            fullname: {
                required : true
            },
            dob:{
                required: true,
                date: true
            },
            gender:{
                required: true
            },
            address:{
                required: true
            },
            phone:{
                required: true
            },
            description:{
                required: true
            }
        }

    })
});
