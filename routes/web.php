<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::Resource('/author','AuthorController')->middleware(['web','auth']);
Route::Resource('/book','BookController')->middleware(['web','auth']);
Route::put('/book/onoff/{id}','BookController@onoff')->middleware(['web','auth'])->name('book.onoff');
Route::get('/book/image/{url}','BookController@getcoverimage')->name('book.image');
Route::put('/author/onoff/{id}','AuthorController@onoff')->middleware(['web','auth'])->name('author.onoff');
